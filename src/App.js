import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {

    return (
        <div className="App">

            {/* Navbar (sit on top) */}
            <div className="navbar">
                <a href="#" className='left'><b>EDTS</b> TDP Batch #2</a>
                {/* Float links to the right. Hide them on small screens */}
                <div className='navbar-menu right'>
                    <a href="#projects">Projects</a>
                    <a href="#about">About</a>
                    <a href="#contact">Contact</a>
                </div>
            </div>

            {/* Header */}
            <header style={{ maxWidth: '1500px' }} id="home">
                <img src={banner} alt="Architecture" width={1500} height={800} />
                <div className='container-title'>
                    <h1 className='text-white'>
                        <span className='initial-title'>
                            <b>EDTS</b>
                        </span>
                        <span className='display-title'>Architects</span>
                    </h1>
                </div>
            </header>

            {/* Page content */}
            <div className='container' style={{ maxWidth: '1564px' }}>
                {/* Project Section */}
                <div id="projects" className='padding-32'>
                    <h3 className='border-bottom padding-top-16 padding-bottom-16'>Projects</h3>
                </div>
                <div className='row' style={{ display: 'flex' }}>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Summer House</div>
                            <img src={house2} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Brick House</div>
                            <img src={house3} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Renovated</div>
                            <img src={house4} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Barn House</div>
                            <img src={house5} alt="House" width={'100%'} />
                        </div>
                    </div>
                </div>
                <div className='row' style={{ display: 'flex' }}>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Summer House</div>
                            <img src={house2} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Brick House</div>
                            <img src={house3} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Renovated</div>
                            <img src={house4} alt="House" width={'100%'} />
                        </div>
                    </div>
                    <div className='col-25 margin-bottom'>
                        <div className='galery-container'>
                            <div className='title background-black padding'>Barn House</div>
                            <img src={house5} alt="House" width={'100%'} />
                        </div>
                    </div>
                </div>

                {/* About Section */}
                <div id="about">
                    <div id="projects" className='padding-32'>
                        <h3 className='border-bottom padding-top-16 padding-bottom-16'>About</h3>
                    </div>
                    <div className='row'>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Excepteur sint
                            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
                <div className='row'>
                    <div style={{ width: '24%', float: 'left', padding: '0 8px' }} >
                        <img src={team1} alt="Jane" width={"100%"} />
                        <h3>Jane Doe</h3>
                        <p>CEO &amp; Founder</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button>Contact</button></p>
                    </div>
                    <div style={{ width: '24%', float: 'left', padding: '0 8px' }} >
                        <img src={team2} alt="John" width={"100%"} />
                        <h3>John Doe</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button>Contact</button></p>
                    </div>
                    <div style={{ width: '24%', float: 'left', padding: '0 8px' }} >
                        <img src={team3} alt="Mike" width={"100%"} />
                        <h3>Mike Ross</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button>Contact</button></p>
                    </div>
                    <div style={{ width: '24%', float: 'left', padding: '0 8px' }} >
                        <img src={team4} alt="Dan" width={"100%"} />
                        <h3>Dan Star</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button>Contact</button></p>
                    </div>
                </div>

                {/* Contact Section */}
                <div id="contact" className='padding-32' style={{ marginTop: '16px', marginBottom: '16px' }}>
                    <h3>Contact</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form action="/" target="_blank">
                        <input class="form-input" type="text" placeholder="Name" required="" name="Name" />
                        <input class="form-input" type="text" placeholder="Email" required="" name="Email" />
                        <input class="form-input" type="text" placeholder="Subject" required="" name="Subject" />
                        <input class="form-input" type="text" placeholder="Comment" required="" name="Comment" />
                        <button class="btn btn-primary" type="submit"> SEND MESSAGE</button>
                    </form>

                    {/***  
           add your element form here 
           ***/}

                </div>

                {/* Image of location/map */}
                <div>
                    <img src={map} alt="maps" width="100%" />
                </div>

                <div id="contact">
                    <h3>Contact List</h3>
                    {/***  
           add your element table here 
           ***/}

                </div>

                {/* End page content */}
            </div>

            {/* Footer */}
            <footer className='center background-black padding-16'>
                <p>Copyright 2022</p>
            </footer>

        </div >
    );
}

export default App;
